from __future__ import unicode_literals
from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models


class User(AbstractBaseUser):
    username = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    name = models.FloatField()
    # next flds ...
    # TODO auth & fields


class AutoType(models.Model):
    description = models.CharField(max_length=100)


class Auto(models.Model):
    number = models.CharField(max_length=20)
    model = models.CharField(max_length=100)
    auto_type = models.ForeignKey(AutoType)


class ContainerType(models.Model):
    capacity = models.IntegerField
    auto_type = models.ForeignKey(AutoType)
    base_cost = models.FloatField()


class Container(models.Model):
    uid = models.CharField(max_length=100)
    container_type = models.ForeignKey(ContainerType)


class UnloadPoint(models.Model):
    description = models.CharField(max_length=2000)
    address = models.CharField(max_length=2000)
    latitude = models.FloatField()
    longitude = models.FloatField()


class CargoType(models.Model):
    description = models.CharField(max_length=100)


class ServiceType(models.Model):
    description = models.CharField(max_length=100)
