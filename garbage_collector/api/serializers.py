from garbage_collector.api.models import *
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'surname', 'name')


class AutoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auto
        fields = ('number', 'model', 'auto_type')


class AutoTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutoType
        fields = ('id', 'description')


class ContainerTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContainerType
        fields = ('capacity', 'auto_type', 'base_cost')


class ContainerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Container
        fields = ('uid', 'container_type')


class UnloadPointSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnloadPoint
        fields = ('description', 'address', 'latitude', 'longitude')


class CargoTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CargoType
        fields = ('id', 'description')


class ServiceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceType
        fields = ('id', 'description')
