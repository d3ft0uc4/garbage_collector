from django.conf.urls import url, include
from rest_framework import routers
from garbage_collector.api import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'autos', views.AutoViewSet)
router.register(r'auto_types', views.AutoTypeViewSet)
router.register(r'container_types', views.ContainerTypeViewSet)
router.register(r'containers', views.ContainerViewSet)
router.register(r'unload_points', views.UnloadPointsViewSet)
router.register(r'cargo_types', views.CargoTypeViewSet)
router.register(r'service_types', views.ServiceTypeViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]
